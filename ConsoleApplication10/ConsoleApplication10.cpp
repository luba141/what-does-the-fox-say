#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "animal voice\n";
	}
};

class Frog: public Animal
{
public:
	void Voice() override
	{
		std::cout << "Croak!\n";
	}
};

class Bird : public Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Tweet!\n";
	}
};

class Cow : public Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Moo!\n";
	}
};

class Dog : public Animal
{
public:
	virtual void Voice()
	{
		std::cout << "woof!\n";
	}
};

class Cat : public Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Meow!\n";
	}
};

class Elephant : public Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Toot!\n";
	}
};

class Fox : public Animal
{
private:
	std::string* sounds;
	int random_variable;
	int const length = 6;
public:
	Fox()
	{
		std::srand(std::time(nullptr));
		sounds = new std::string[length];

		sounds[0] = "Ring-ding-ding-ding-dingeringeding!";
		sounds[1] = "Wa-pa-pa-pa-pa-pa-pow!";
		sounds[2] = "Hatee-hatee-hatee-ho!";
		sounds[3] = "Joff-tchoff-tchoffo-tchoffo-tchoff!";
		sounds[4] = "Jacha-chacha-chacha-chow!";
		sounds[5] = "Wa-wa-way-do, wub-wid-bid-dum-way-do, wa-wa-way-do";
	}

	void Voice() override
	{
		random_variable = std::rand();
		std::cout << sounds[random_variable% length] << '\n';
	}
};

int main()
{
	Animal* animals[] = {new Frog(), new Bird(), 
		new Dog(), new Elephant(), new Cat(), new Fox()};
	for (auto elem: animals)
	{
		elem->Voice();
	}
	return 0;
}